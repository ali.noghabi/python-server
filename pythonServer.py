import asyncio
import websockets

# List of connected clients
clients = set()

async def handle_websocket(websocket, path):
    # Add the client to the list of connected clients
    clients.add(websocket)
    try:
        while True:
            message = await websocket.recv()
            print(f"Received message: {message}")
            response = ""
            if(str(message).startswith("url:")):
                response = message
            else:    
                response = "Received your message: " + message
            # Broadcast the message to all connected clients
            for client in clients:
                if(len(clients) < 2):
                    await client.send("your the only client in server")
                else:
                    await client.send(response)
    except websockets.exceptions.ConnectionClosedOK:
        # Remove the client from the list of connected clients
        clients.remove(websocket)
        print("WebSocket connection closed.")

start_server = websockets.serve(handle_websocket, 'localhost', 5000)
print("WebSocket server started...")
try:
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
finally:
    start_server.close()
    asyncio.get_event_loop().run_until_complete(start_server.wait_closed())