# python-server
- python web server: get request clients and send the response to all clients
- python client: get url from server and send results to server
- javaScript client: send url to server and get results from server
# setup
- install websockets and python
```
pip install websockets
```
- clone project
- run Python server
- run Python client
- open index.html in browser and type an url then press send button and see results.
