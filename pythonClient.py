import asyncio
import websockets


async def send_string_to_websocket(websocket, string):
    await websocket.send(string)
    response = await websocket.recv()
    print(f"Received response: {response}")


async def main():
    async with websockets.connect('ws://localhost:5000') as websocket:
        response = await websocket.recv()
        print(f"url recived: {response}")
        if (str(response).startswith("url:")):
            async with websockets.connect('ws://localhost:5000') as websocket:
                # Call this function to send the string to the WebSocket server
                await send_string_to_websocket(websocket, "python client send message 1")
                await asyncio.sleep(1)
                await send_string_to_websocket(websocket, "python client send message 2")
                await asyncio.sleep(1)
                await send_string_to_websocket(websocket, "python client send message 3")
                await asyncio.sleep(1)
                await send_string_to_websocket(websocket, "python client send message 4")
                await asyncio.sleep(1)
                await send_string_to_websocket(websocket, "python client send message 5")
        else:
            print("not a url")
# Run the event loop
asyncio.get_event_loop().run_until_complete(main())
